package com.tmobile.android_054.vm

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tmobile.android_054.apiService.UrbanDicSvc
import com.tmobile.android_054.model.ListItem
import com.tmobile.android_054.model.ResponseForDefineCall

class UrbanDicVM (application: Application, scheduler: Scheduler): ABCVM(application, scheduler) {

    val query = ABCVM.AioB<String, ResponseForDefineCall>(this, "UrbanDicVM.query") {
        UrbanDicSvc.define(it)
    }.also {
        it._res.observeForever {
            //here we are going to update ui, if it contains
            //something essential...
            if (it.list.isNotEmpty()) {
                list.clear()
                it.list.forEach { list.add(it)

                Log.d(TAG, "$it")
                }
            }
        }
    }

    val list: ObservableArrayList<ListItem> by lazy {
        ObservableArrayList<ListItem>()
    }




    @Suppress("UNCHECKED_CAST")
    class Factory(val application: Application, val scheduler: Scheduler) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(UrbanDicVM::class.java)) {
                return UrbanDicVM(application, scheduler) as T
            }
            throw IllegalArgumentException("${UrbanDicVM::class.java.simpleName}.Factory: " +
                    "Unknown modelClass: ${modelClass::class.java.simpleName}")
        }
    }
    companion object {
        val TAG = UrbanDicVM::class.java.simpleName

    }

}
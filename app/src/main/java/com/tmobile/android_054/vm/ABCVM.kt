package com.tmobile.android_054.vm


import android.app.Application
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import kotlin.collections.set

//TODO: THE SAME LOGIC SUPPOSE TO BE IMPLEMENTED FOR CO-ROUTINES.
open class ABCVM : AndroidViewModel, Bindable {

    private val scheduler: Scheduler//SchedulerProduction()


    constructor(
        _application: Application,
        _scheduler: Scheduler//WE NEED SCHEDULER FOR CHANGING MAIN THREAD DURING UNIT TESTING
        //NB!!                                 variant for co-routines does not need scheduler
    ) : super(_application) {
        scheduler = _scheduler
    }

    /** ________________________________________________________________________________________ AfB
     * This is a basic building block for andy view model. It connects the
     * MutableLiveData<A> with MutableLiveData<B>. Whenever A retrieves
     * new values, the block processes function f on the io thread
     * this function must return value of class B to update MutableLIveData<B>
     *
     */
    open class AioB <A,B> {
        var isInit = false
        fun initAndObserveRes(value: A? = null, f: (b: B) -> Unit) {
            isInit = true
            if (value != null) {
                this._start.value = value
            }
            this._res.observeForever {
                if (!isInit) {
                    f(it)
                }
                isInit = false
            }
        }
        fun init(a: A) {
            isInit = true
            this._start.value = a

        }

        val f: (m: A) -> B
        val tag: String
        constructor(bindable: Bindable, tag: String, debounce:Int =100, f: (m: A) -> B) {
            this.tag = tag
            this.f = f
            this._start = MutableLiveData<A>()
            this._res = MutableLiveData<B>()
            this.debounce = debounce
            bindable.bind(this)
        }
        constructor(start:MutableLiveData<A>, res: MutableLiveData<B>,
                    bindable: Bindable, tag: String, debounce: Int=100, f: (m: A) -> B) {
            this.tag = tag
            this.f = f
            this._start = start
            this._res = res
            this.debounce = debounce
            bindable.bind(this)
        }

        val _start: MutableLiveData<A>
        val _res: MutableLiveData<B>
        val debounce : Int
    }
    class AmainB<A,B> {

        val f: (m: A) -> B
        val tag: String
        val _start: MutableLiveData<A>
        val _res: MutableLiveData<B>
        val debounce:Int

        constructor(bindable: Bindable, tag: String, debounce:Int = 100, f:(m:A)->B) {
            this.tag = tag
            this.f = f
            this._start = MutableLiveData<A>()
            this._res = MutableLiveData<B>()
            this.debounce = debounce
            bindable.bind(this)
        }

    }

    /**__________________________________________________________________________________AmainB_C
    setup observable in the constructor for MutableLiveData <A>,
    and execute function f, if the result is B, assign it to b,
    if the result is C, assign it to c
    This code is applicable only for MainThread
     */
    class AmainB_C<A,B,C> {
        val a: MutableLiveData<A>
        val b: MutableLiveData<B>?
        val c: MutableLiveData<C>?
        val f: (m: A) -> L
        val observer:Observer<A>

        constructor(
            a: MutableLiveData<A>,
            b: MutableLiveData<B>?,
            c: MutableLiveData<C>?,
            f: (m: A) -> L
        ) {
            this.a = a
            this.b = b
            this.c = c
            this.f = f
            this.observer  = Observer{ value->
                val l = f(value)
                when ( l.genericAbbr ) {
                    "B" -> b?.postValue( l.res as B)
                    "C" -> c?.postValue( l.res as C)
                }
            }
            this.a.observeForever(observer)
        }
        fun dispose() {
            a.removeObserver(observer)
        }
    }
    /**________________________________________________________________________________________A_B1fC
     * observes A and B and triggers only the first one event.
     * It MUST be called dynamically,
     * every time, whenever we need to figure out which response was first
     */
    open class A_B1mainC<A,B>//
        (
        private val a: MutableLiveData<A>,
        private val b: MutableLiveData<B>?,
        private val f: (a: A?, b: B?) -> Unit
    ) {
        private val observerA: Observer<A>
        private val observerB: Observer<B>
        private var cnt = 0
        private var cntMax = 1
        init {
            a.value?.let { a ->
                cntMax ++
            }
            b?.value?.let { b ->
                cntMax ++
            }
            this.observerA = Observer<A> { value->
                if (++cnt == cntMax) {
                    f(value, null)
                    dispose()
                }
            }
            this.observerB = Observer<B> {value->
                if (++cnt == cntMax) {
                    f(null, value)
                    dispose()
                }
            }
            this.a.observeForever(observerA)
            this.b?.observeForever(observerB)
        }

        private fun dispose() {
            a.removeObserver(observerA)
            b?.removeObserver(observerB)
        }
    }

    /**__________________________________________________________________________________A1B
     * connects A and B, as soon as A is being triggered, it passes the
     * value to the B. Function runs once, and MUST be called dynamically
     *
     */
    class A1B<A,B> : A_B1mainC<A, B> {
        private val a: MutableLiveData<A>
        private val b: MutableLiveData<B>
//        private val value: B

        constructor(a: MutableLiveData<A>, b: MutableLiveData<B>, value: B) : super(
            a, null,
            { _a, _b ->
                _a?.let {
                    b.value = value
                }
            }) {
            this.a = a
            this.b = b
        }


        //this constructor allows us to link two mutableLiveData objects
        //and run the connection once.
        constructor (a: MutableLiveData<A>, b: MutableLiveData<B>) : super (
            a, null,
            { _a, _b ->
                _a?.let {
                    b.value = it as B
                }
            }) {
            this.a = a
            this.b = b
        }
        //link once and convert parameter A into B using function f
        constructor (_a: MutableLiveData<A>, b: MutableLiveData<B>, f: (m: A) -> B): super (
            _a, null,  {_a, _b ->
                _a?.let {
                    b.value = f(it)
                }
            }
        ) {
            this.a = _a
            this.b = b
        }

    }


    private val vmDisposable = CompositeDisposable()

    override fun onCleared() {
        vmDisposable.dispose()
        super.onCleared()
    }


    /**_______________________________________________________________________________________AioB_C
    setup Link in the constructor for MutableLiveData <A>,
    and execute function f, if the result is B, assign it to b,
    if the result is C, assign it to c
    This code is applicable for IO thread
     */
    class AioB_C<A,B,C> {
        val a: MutableLiveData<A>
        val a_res: MutableLiveData<A>
        val b: MutableLiveData<B>?
        val c: MutableLiveData<C>?
        val f: (m: A) -> L
//        val observer:Observer<A>// we don't need observer

        constructor(
            tag: String,
            a: MutableLiveData<A>,
            b: MutableLiveData<B>?,
            c: MutableLiveData<C>?,
            bindable: Bindable,
            debounce:Int = 100,
            f: (m: A) -> L

        ) {
            this.a = a//MutableLiveData<A>()
            this.a_res = MutableLiveData<A>()


            this.b = b
            this.c = c
            this.f = f

            //here we can't use generics to make bindding with BindableViewModel
            val garbage = Garbage().also {garbage->

                garbage.src = a
                garbage.tag = tag
                garbage.observer = Observer<A> {
                }

                garbage.disp = Observable.create<A> { emitter ->

                    emitter.setCancellable { Log.e( TAG, "________________________________$tag ->>>> EMITTER WAS DESTROYED!!!" ) }

                    val observer = Observer<A> {
                        Log.d(TAG, " emitter (${emitter.toString()})->  ${it.toString()}")
                        emitter.onNext(it)
                    }
                    garbage.observer = observer
                    garbage.emitter = emitter
                    a.observeForever(observer)
                }
                    .subscribeOn(bindable.scheduler().mainThread())
                    .also {
                        if (debounce >= 0) {
                            it.debounce(debounce.toLong(), TimeUnit.MILLISECONDS )//can be good for UI, make it as a parameter
                        }
                    }
//                    .debounce(100, TimeUnit.MILLISECONDS )//can be good for UI, make it as a parameter
                    .observeOn(Schedulers.io())//Schedulers.io()
                    .flatMap { a ->
                        //it goes to be in Schedulers.io, we can call retrofit...
                        val l = f(a)
                        //depends on res post value to B or C:
                        when (l.genericAbbr) {
                            "B" -> b?.postValue( l.res as B)
                            "C" -> c?.postValue( l.res as C)
                        }
                        return@flatMap Observable.just(a)//return the same value, we should usually use it
                    }
                    .subscribe(
                        { d ->
                            a_res.postValue(d)
                        }, { e ->
                            Log.e(tag, e.message, e )
                        }, {}
                    )
            }
            bindable.garbageMap()[a] = garbage
            bindable.vmDisposable().addAll(garbage.disp)
        }

    }
    //syntax sugar for objects of class L
    data class A<A>(val a: A):L("A", a as Any)
    data class B<B>(val b: B):L("B", b as Any)
    data class C<C>(val c: C):L("C", c as Any)

    //This is a very straight forwarded way how to discriminate differences in classes.
    //Assume, that whole framework will use only A, B and C names for generics, we
    //actually need to make a distinguish between A and B or B and C, but!
    //in cases, when A actually == B, this method gives us a clear difference between
    //generics EVEN they could be equal.
    open class L(var genericAbbr:String, var res:Any) {
        fun set(genericAbbr:String, res:Any) {
            this.genericAbbr = genericAbbr
            this.res = res
        }
    }

    override fun <A, B> bind(aioB: AioB<A, B>) {
        bind(aioB.tag, aioB._start, aioB._res, true, aioB.debounce, aioB.f )
    }

    override fun <A, B> bind(amainB: AmainB<A, B>) {
        bind(amainB.tag, amainB._start, amainB._res, false, amainB.debounce, amainB.f)
    }

    override fun scheduler(): Scheduler {
        return this.scheduler
    }

    override fun garbageMap(): HashMap<MutableLiveData<*>, Garbage> {
        return garbageMap
    }

    override fun vmDisposable(): CompositeDisposable {
        return vmDisposable
    }

    val mainHandler = Handler(Looper.getMainLooper())


    //here we are expecting the first argument, that was bind
    //in couple.
    fun unbind(vararg srcs: MutableLiveData<*>) {

        val runnable = Runnable {
            for (src in srcs) {
                val d = garbageMap[src]
                d?.let {
                    try {
                        //1. dispose value from vmDisposable
                        d.emitter.setCancellable {
                            Log.d( TAG, "________________________________${d.tag} - unbind >>>> emitter successfully destroyed" )
                        }
                        d.disp.dispose()
                        //2. delete observer, linked to src
                        d.src.removeObserver(d.observer as Observer<Any>)
                    } catch (e: Exception) {
                        Log.e(TAG, "unbind: ", e)
                    }
                }
            }
        }
        mainHandler.post(runnable)
    }
    override fun runInMainThread(r: Runnable) {
        mainHandler.post(r)
    }

    class Garbage  {
        lateinit var disp: Disposable
        lateinit var src:MutableLiveData<*>
        lateinit var observer: Observer<*>
        lateinit var emitter: ObservableEmitter<*>
        lateinit var tag:String
    }
    private val garbageMap = HashMap<MutableLiveData<*>, Garbage>()


    fun <A, B> bind(
        tag: String,
        src: MutableLiveData<A>,
        dst: MutableLiveData<B>?,
        isIo:Boolean,
        debounce:Int = 100,
        f: (m: A) -> B
    ) {

        if (!isIo) {
            //just connect src, dst through function f
            src.observeForever { a->
                dst?.value = f(a)
            }
            return
        }

        val garbage = Garbage().also {garbage->

            garbage.src = src
            garbage.tag = tag
            garbage.observer = Observer<A> {
            }

            garbage.disp = Observable.create<A> { emitter ->

                emitter.setCancellable {
                    Log.e( TAG, "________________________________$tag ->>>> EMITTER WAS DESTROYED!!!" )
                }

                val observer = Observer<A> {
                    Log.d(TAG, " emitter (${emitter.toString()})->  ${it.toString()}")
                    emitter.onNext(it)
                }
                garbage.observer = observer
                garbage.emitter = emitter
                src.observeForever(observer)
            }
                .subscribeOn(scheduler.mainThread())
                .also {
                    if (debounce >= 0) {
                        it.debounce(debounce.toLong(), TimeUnit.MILLISECONDS )//can be good for UI, make it as a parameter
                    }
                }
//                .debounce(100, TimeUnit.MILLISECONDS )//can be good for UI, make it as a parameter
                .observeOn(Schedulers.io())
                .flatMap { s ->
                    return@flatMap Observable.just(f(s))
                }

                .subscribe(
                    { d ->
                        dst?.postValue(d)
                    }, { e ->
                        Log.e(tag, e.message, e )
                    }, {}
                )
        }
        garbageMap[src] = garbage
        vmDisposable.addAll(garbage.disp)
    }

    companion object {
        private val TAG = ABCVM::class.java.simpleName
    }
    interface Scheduler {
        fun mainThread (): io.reactivex.Scheduler
    }

    class SchedulerProduction : Scheduler {
        override fun mainThread(): io.reactivex.Scheduler = AndroidSchedulers.mainThread()
    }

    class SchedulerTesting : Scheduler {
        override fun mainThread(): io.reactivex.Scheduler = Schedulers.trampoline()
    }

}
interface Bindable {
    fun <A, B> bind(aioB: ABCVM.AioB<A,B>)
    fun <A, B> bind(amainB: ABCVM.AmainB<A,B>)
    fun scheduler(): ABCVM.Scheduler
    fun garbageMap():HashMap<MutableLiveData<*>, ABCVM.Garbage>
    fun vmDisposable(): CompositeDisposable
    fun runInMainThread(r:Runnable)
}
package com.tmobile.android_054

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.tmobile.android_054.databinding.ActivityMainBinding
import com.tmobile.android_054.vm.ABCVM
import com.tmobile.android_054.vm.UrbanDicVM

class MainActivity : AppCompatActivity() {

    val urbanDicVM by viewModels<UrbanDicVM> {
        UrbanDicVM.Factory(application, ABCVM.SchedulerProduction())}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.urbanDicVM = urbanDicVM


        binding.root.findViewById<RecyclerView>(R.id.rv).also {

        }

        urbanDicVM.query._start.value = "hello"

    }
}
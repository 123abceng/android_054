package com.tmobile.android_054.apiService


import com.tmobile.android_054.api.UrbanDicApi
import com.tmobile.android_054.model.ResponseForDefineCall

object UrbanDicSvc {

    fun define(term: String): ResponseForDefineCall {
        val res = UrbanDicApi.instance().define(term).execute().body()
        res?.let {
            return res
        }
        return ResponseForDefineCall(listOf())
    }

}
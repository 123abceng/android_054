package com.tmobile.android_054.api


import com.google.gson.GsonBuilder
import com.tmobile.android_054.model.ResponseForDefineCall
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface UrbanDicApi {

    @GET("define")
    fun define(@Query("term") term: String = "hello"
    ): Call<ResponseForDefineCall>

    companion object Factory {

        private val API_KEY = "e881d4bf5dmsh6f05c67f768c9dcp1600c3jsnc6e11130a899"
        private val BASE_DOMAIN = "mashape-community-urban-dictionary.p.rapidapi.com"
        private val BASE_URL = "https://$BASE_DOMAIN"

        private fun create(): UrbanDicApi {

            val client = OkHttpClient.Builder()
                .addInterceptor{it.proceed(it.request().newBuilder()
                    .addHeader("x-rapidapi-host", BASE_DOMAIN)
                    .addHeader("x-rapidapi-key", API_KEY).build())}
                .build()

            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()

                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit.create(UrbanDicApi::class.java)
        }
        private val service : UrbanDicApi by lazy { create() }

        fun instance(): UrbanDicApi {
            return service
        }

    }
}
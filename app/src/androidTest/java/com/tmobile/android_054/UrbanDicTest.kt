package com.tmobile.android_054

import android.app.Application
import androidx.annotation.UiThread
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.tmobile.android_054.api.UrbanDicApi
import com.tmobile.android_054.vm.ABCVM
import com.tmobile.android_054.vm.UrbanDicVM

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class UrbanDicTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var urbanDicApi: UrbanDicApi
    lateinit var urbanDicVM: UrbanDicVM


    @Before
    fun initTest() {

        urbanDicApi = UrbanDicApi.instance()

        urbanDicVM = UrbanDicVM(Application(), ABCVM.SchedulerTesting())
    }

    @Test
    fun testCall() {

        val response = urbanDicApi.define().execute()
        assertTrue(response.isSuccessful)
        assertTrue(response.body() != null)
        response.body()?.let {
            assertTrue(it.list != null)
            it.list?.let {
                assertTrue(it.size != 0)
            }
        }

    }


    @Test

    fun testVM() {
        val done = AtomicBoolean(false)
        urbanDicVM.query._res.observeForever{ t -> done.set(true) }

        urbanDicVM.query._start.postValue("test")

        while (!done.get()) { Thread.sleep(1) }

        assertTrue(urbanDicVM.list.size > 0)
    }





}